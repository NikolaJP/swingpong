package main;

import javax.swing.*;
import java.awt.*;

public class Pong {
    public static void main(String[] args) {
        final int SCREEN_WIDTH = 600;
        final int SCREEN_HEIGHT = 600;
        int rPaddleW = 15;
        int rPaddleH = 150;
        int rPaddleY = SCREEN_HEIGHT / 2 - rPaddleH / 2;
        int rPaddleX = SCREEN_WIDTH - rPaddleW;
        int lPaddleY = SCREEN_HEIGHT / 2 - rPaddleH / 2;
        int lPaddleX = 0;


        JFrame frame = new JFrame();
        Paddle rightPaddle = new Paddle(rPaddleX, rPaddleY, rPaddleH, rPaddleW);
        Paddle leftPaddle = new Paddle(lPaddleX, lPaddleY, rPaddleH, rPaddleW);
        Player playerR = new Player();
        Player playerL = new Player();

        Ball ball = new Ball();


        Game game = new Game(rightPaddle, leftPaddle, ball, playerR, playerL, SCREEN_WIDTH, SCREEN_HEIGHT);
        frame.add(game);
        frame.setSize(SCREEN_WIDTH + 15, SCREEN_HEIGHT + 35);
        frame.getContentPane().setBackground(Color.black);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        if (game.getPlayerR().getLives() <= 0) {
            System.exit(0);

        }


    }
}
