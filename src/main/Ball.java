package main;

import java.util.Random;

public class Ball {
    Random random = new Random();

    private double x = 300;
    private double dirX;
    private double y = 300;
    private double dirY;
    private double ax = random();

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void checkCollisionTB(double y, int height) {

        if (y <= 0 || y >= height - 20) {
            dirY *= -1;
        }
    }

    public boolean checkCollisionL(double x) {

        return (x <= 0);
    }

    public boolean checkCollisionR(double x, int width) {

        return (x >= width - 20);
    }

    public void bounceOffPaddle(int rpX, int rpY, int rpH, int rpW, int lpX, int lpY, int lpH, int lpW, int sW) {
        double difference;
        double k;
        double radians = 4 * Math.PI / 12;
        double angle;
        if (((y + dirY >= rpY && y + dirY <= rpY + rpH) && (x + dirX + 20 >= rpX))
                || ((y + dirY >= lpY && y + dirY <= lpY + lpH) && (x + dirX <= lpX + 15))) {

            //if it hits the right paddle
            if ((y + dirY >= rpY && y + dirY <= rpY + rpH) && (x + dirX + 20 >= rpX)) {
                //calculate where it hit the paddle and set a bouncing angle depending on it
                difference = (rpY + ((double) rpH / 2)) - (y);
                k = difference / ((double) rpH / 2);
                angle = k * radians;
                dirX = -Math.cos(angle) * 3;
                dirY = Math.sin(angle) * 3;
            }
            //if it hits the left paddle
            if (x <= lpX + 15) {
                //calculate where it hit the paddle and set a bouncing angle depending on it

                difference = (lpY + ((double) lpH / 2)) - (y);
                k = difference / ((double) lpH / 2);
                angle = k * radians;
                dirX = Math.cos(angle) * 3;
                dirY = -Math.sin(angle) * 3;
            }


        }
        //checks if top or bottom of the paddle are hit
        if ((x + dirX + 10 >= sW - rpW && x + dirX + 20 <= sW) && (y + 20 + dirY >= rpY && y + dirY <= rpY + rpH)) {
            difference = (rpY + ((double) rpH / 2)) - (y);
            k = difference / ((double) rpH / 2);
            angle = k * radians;
            dirX = -Math.cos(angle) * 3;
            dirY = Math.sin(angle) * 3;
        }
        if ((x + dirX >= 0 && x + dirX <= lpW) && (y + dirY + 20 >= lpY && y + dirY <= lpY + lpH)) {
            difference = (lpY + ((double) lpH / 2)) - (y);
            k = difference / ((double) lpH / 2);
            angle = k * radians;
            dirX = Math.cos(angle) * 3;
            dirY = -Math.sin(angle) * 3;
        }

    }

    //initialize the ball, set its starting position and direction
    public void start(int height, int width) {
        x = (double) width / 2 - 10;
        y = (double) height / 2 - 10;
        directionX(3, ax);
        directionY(3, ax);
    }


    public void move() {
        x += dirX;
        y += dirY;
    }


    public void directionX(int speed, double ax) {

        dirX = speed * Math.cos(ax);
        double r = random.nextDouble();
        if (r >= 0.5) {
            dirX *= -1;
        }

    }

    public void directionY(int speed, double ax) {

        dirY = speed * Math.sin(ax);
        double r = random.nextDouble();
        if (r >= 0.5) {
            dirY *= -1;
        }

    }

    public double random() {


        Random random = new Random();
        double pi = Math.PI;
        double ax = -pi / 4 + (pi / 4 - (-pi / 4)) * random.nextDouble();
        if (3 * Math.cos(ax) == 0) {
            ax = -pi / 4 + (pi / 4 - (-pi / 4)) * random.nextDouble();
        }
        if (3 * Math.sin(ax) == 0) {
            ax = -pi / 4 + (pi / 4 - (-pi / 4)) * random.nextDouble();
        }


        return ax;
    }

    public double map(double x, double in_min, double in_max, double out_min, double out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }


}
