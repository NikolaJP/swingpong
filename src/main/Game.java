package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;

public class Game extends JPanel implements ActionListener, KeyListener {
    Timer timer = new Timer(5, this);
    boolean isPlaying = false;

    Paddle rightPaddle;
    Paddle leftPaddle;
    Ball ball;
    private Player playerR;

    public Player getPlayerR() {
        return playerR;
    }


    private Player playerL;


    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;

    public Game(Paddle rightPaddle, Paddle leftPaddle, Ball ball, Player playerR, Player playerL, int SCREEN_WIDTH, int SCREEN_HEIGHT) {
        if (isPlaying) {
            timer.start();
        }
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        this.rightPaddle = rightPaddle;
        this.leftPaddle = leftPaddle;
        this.ball = ball;
        this.playerR = playerR;
        this.playerL = playerL;
        ball.start(SCREEN_HEIGHT, SCREEN_WIDTH);


        this.SCREEN_HEIGHT = SCREEN_HEIGHT;
        this.SCREEN_WIDTH = SCREEN_WIDTH;
    }

    public void paint(Graphics g) {
        int fontSize = 30;
        Font f = new Font("Helvetica", Font.BOLD, fontSize);

        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setFont(f);
        graphics2D.setBackground(Color.black);
        graphics2D.setColor(Color.gray);
        graphics2D.drawLine(SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2, SCREEN_HEIGHT);
        graphics2D.setColor(Color.black);
        graphics2D.drawString(playerR.getLife(), SCREEN_WIDTH / 2 + 30, 30);
        graphics2D.drawString(playerL.getLife(), SCREEN_WIDTH / 2 - 60, 30);
        graphics2D.fill(new Rectangle(SCREEN_WIDTH - rightPaddle.getWidth(), rightPaddle.getY(), rightPaddle.getWidth(), rightPaddle.getHeight()));
        graphics2D.fill(new Rectangle(leftPaddle.getX(), leftPaddle.getY(), leftPaddle.getWidth(), leftPaddle.getHeight()));
        graphics2D.setColor(Color.red);
        graphics2D.fill(new Ellipse2D.Double(ball.getX(), ball.getY(), 20, 20));
        graphics2D.setColor(Color.BLACK);
        if (!isPlaying) {
            graphics2D.drawString("Press SPACE for new game", 100, 200);
        }
        if (playerL.getLives() <= 0 && playerR.getLives() > 0) {
            graphics2D.drawString("Right Player won!", SCREEN_WIDTH / 2 + 30, SCREEN_HEIGHT / 2 + 100);
            System.out.println("right");

        } else if (playerL.getLives() > 0 && playerR.getLives() <= 0) {
            graphics2D.drawString("Left Player won!", 30, SCREEN_HEIGHT / 2 + 100);
            System.out.println("left");

        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
        rightPaddle.move();
        leftPaddle.move();

        // prevent the paddle of going off screen
        rightPaddle.stayInBounds(SCREEN_HEIGHT);
        leftPaddle.stayInBounds(SCREEN_HEIGHT);
        //check ball
        ball.checkCollisionTB(ball.getY(), SCREEN_HEIGHT);
        if (ball.checkCollisionL(ball.getX())) {
            playerL.loseLife();
            playerL.changeScore();
            ball.start(SCREEN_HEIGHT, SCREEN_WIDTH);
        } else if (ball.checkCollisionR(ball.getX(), SCREEN_WIDTH)) {
            playerR.loseLife();
            playerR.changeScore();
            System.out.println(playerR.getLives());
            ball.start(SCREEN_HEIGHT, SCREEN_WIDTH);
        }
        ball.bounceOffPaddle(rightPaddle.getX(), rightPaddle.getY(), rightPaddle.getHeight(), rightPaddle.getWidth(), leftPaddle.getX(), leftPaddle.getY(), leftPaddle.getHeight(), leftPaddle.getWidth(), SCREEN_WIDTH);


        ball.move();
        //stop game if one player has lost
        if (playerL.getLives() <= 0 || playerR.getLives() <= 0) {
            timer.stop();
            isPlaying = false;

        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_UP) {
            rightPaddle.moveUp();
        }
        if (code == KeyEvent.VK_DOWN) {
            rightPaddle.moveDown();
        }
        if (code == KeyEvent.VK_W) {
            leftPaddle.moveUp();
        }
        if (code == KeyEvent.VK_S) {
            leftPaddle.moveDown();
        }
        if (code == KeyEvent.VK_SPACE && !isPlaying) {
            //restart the game and reset lives
            timer.start();
            ball.start(SCREEN_HEIGHT, SCREEN_WIDTH);
            playerL.setLives(10);
            playerR.setLives(10);
            leftPaddle.setY(SCREEN_HEIGHT / 2 - leftPaddle.getHeight() / 2);
            rightPaddle.setY(SCREEN_HEIGHT / 2 - rightPaddle.getHeight() / 2);
            isPlaying = true;
            playerL.setLife("10");
            playerR.setLife("10");
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_UP || code == KeyEvent.VK_DOWN) {
            rightPaddle.stop();
        }
        if (code == KeyEvent.VK_W || code == KeyEvent.VK_S) {
            leftPaddle.stop();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
        //empty because there isn't a need for it
    }


}
