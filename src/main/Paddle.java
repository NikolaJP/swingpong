package main;

public class Paddle {
    private int x;
    private int dirY;
    private int y;
    private int height;
    private int width;


    public void setDirX(int dirY) {
        this.dirY = dirY;
    }


    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHeight() {
        return height;
    }


    public int getWidth() {
        return width;
    }


    public Paddle(int x, int y, int height, int width) {
        this.height = height;
        this.width = width;
        this.x = x;
        this.y = y;
    }

    public void moveUp() {
        setDirX(-3);
    }

    public void moveDown() {
        setDirX(3);
    }

    public void stop() {
        setDirX(0);
    }

    public void move() {
        y += dirY;
    }

    //check if paddle is in bounds
    public void stayInBounds(int sHeight) {
        if (y + dirY <= 0) {
            y = 0;
        } else if (y + dirY + height >= sHeight) {
            y = sHeight - height - dirY;
        }
    }


}
